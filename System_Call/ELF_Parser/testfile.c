#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

int main()
{
	char movie1[] = "movie 1";

	char * movie2 = "movie pointer";

	puts(movie1);
	puts(movie2);


	// movie1 = "movie movie"; // can't do this because array type
	movie2 = "pointer movie";

	puts(movie2);
	return 0;
}
