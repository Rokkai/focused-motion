/*
 * ELF Parser:
 *     File handling with system calls.
 *     This program will open a file (passed in through command line).
 *     It will check the file and then close it.
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <elf.h>


//#define DEBUG


/****************************************************************
 * Function: usage()
 * Brief: Check that a single argument was passed in
 ****************************************************************/
void usage(int argc)
{
	if (argc != 2)
	{
		fprintf(stderr, "\nThis program takes in one argument-"
			"the file it's meant to read.\n");
		exit(1);
	}
}

/***************************************************************
 * Function: main()
 * Brief: Read a binary file
 ***************************************************************/
int main(int argc, char *argv[])
{
	int fd = 0;
	int sz = 0;

	usage(argc);

	#ifdef DEBUG
	printf("\nDEBUG: Check input file = %s\n", argv[1]);
        printf("DEBUG: Check number of args. argc = %d\n", argc);
        printf("DEBUG: argv[0] = %s\n", argv[0]);
        printf("DEBUG: argv[1] = %s\n", argv[1]);
	#endif

	// create memory space to read the ELF header
	void *buf = calloc(1, sizeof(Elf64_Ehdr)); 

	// open the file to be read
	fd = open(argv[1], O_RDONLY);

	if (fd == -1) // if the file didn't open fd will be -1)
	{
		perror("cannot open file");
		exit(1);
	}
	#ifdef DEBUG
	printf("DEBUG: Opened the file designator. (fd = % d)\n", fd);
	#endif

	// read the Elf header from the ELF file
	sz = read(fd, buf, sizeof(Elf64_Ehdr));
	
	#ifdef DEBUG
	printf("DEBUG: Check of sz = %c\n", sz);
	printf("DEBUG: Called read (%d, buf, sizeof(Elf64_Ehdr). returned that"
	       " %d bytes were read.\n", fd, sz);
	#endif


	Elf64_Ehdr *pE = (Elf64_Ehdr *)buf;

	/*******************************************************************
	 * Print statement - This will be moved into its own function later
	 ******************************************************************/
	printf("Elf Header:\n");
	

	// MAGIC
	printf("  Magic:	%.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x %.02x\n", pE->e_ident[0], pE->e_ident[1], pE->e_ident[2], pE->e_ident[3], pE->e_ident[4], pE->e_ident[5], pE->e_ident[6], pE->e_ident[7], pE->e_ident[8], pE->e_ident[9], pE->e_ident[10], pE->e_ident[11], pE->e_ident[12], pE->e_ident[13], pE->e_ident[14], pE->e_ident[15]);

	// CLASS
	if (pE->e_ident[4] == 1)
	{
		printf("  Class:				ELF32\n");
	}
	else if (pE->e_ident[4] == 2)
	{
		printf("  Class:				ELF64\n");
	}
	else
	{
		printf("  Class:				This class is invalid\n");
	}
	
	// DATA
	if (pE->e_ident[5] == 1)
	{
		printf("  Data:					2's complement, little endien\n");
	}
	else if (pE->e_ident[5] == 2)
	{
		printf("  Data:					2's complement, big endien\n");
	}
	else
	{
		printf("  Data:					Unknown data format\n");
	}

	// VERSION (ELF Specification)
	if (pE->e_ident[6] == 1)
	{
		printf("  Version (ELF Specification):		1 (current)\n");
	}
	else
	{
		printf("  Version (Elf Specification):		Invalid version\n");
	}

	// DATA	
	if (pE->e_ident[7] <= 1)
	{
		printf("  OS/ABI:				UNIX System V ABI\n");
	}
	else if (pE->e_ident[7] == 2)
	{
		printf("  OS/ABI:				HP-UX ABI\n");
	}
	else if (pE->e_ident[7] == 3)
	{
		printf("  OS/ABI:				NetBSD ABI\n");
	}
	else if (pE->e_ident[7] == 4)
	{
		printf("  OS/ABI:				Linux ABI\n");
	}
	else if (pE->e_ident[7] == 5)
	{
		printf("  OS/ABI:				Solaris ABI\n");
	}
	else if (pE->e_ident[7] == 6)
	{
		printf("  OS/ABI:				IRIX ABI\n");
	}
	else if (pE->e_ident[7] == 7)
	{
		printf("  OS/ABI:				FreeBSD ABI\n");
	}
	else if (pE->e_ident[7] == 8)
	{
		printf("  OS/ABI:				TRU64 UNIX ABI\n");
	}
	else if (pE->e_ident[7] == 9)
	{
		printf("  OS/ABI:				ARM architecture ABI\n");
	}
	else if (pE->e_ident[7] == 10)
	{
		printf("  OS/ABI:				Stand-alone (embedded) ABI\n");
	}

	// ABI Version
	printf("  ABI Version:				%d\n", pE->e_ident[8]);

	// TYPE
	switch (pE->e_type)
	{
		case 0:
			printf("  Type:					NONE (Unknown Type)\n");
			break;
		case 1:
			printf("  Type:					REL (Relocatable File)\n");
			break;
		case 2:
			printf("  Type:					EXEC (Executable File)\n");
			break;
		case 3:
			printf("  Type:					DYN (Shared Object)\n");
			break;
		case 4:
			printf("  Type:					CORE (Core File)\n");
			break;
	}

	// Machine
	switch (pE->e_machine)
	{
		case 0:
			printf("  Machine:				Unknown Machine\n");
			break;
		case 1:
			printf("  Machine:				AT&T WE 32100\n");
			break;
		case 2:
			printf("  Machine:				SPARC\n");
			break;
		case 3:
			printf("  Machine:				Motorola 68000\n");
			break;
		default:
			printf("  Machine Value:			%d\n", pE->e_machine);
	}

	// Version (File)
	switch (pE->e_version)
	{
		case 0:
			printf("  Version (File):			None (Invalid)\n");
			break;
		case 1:
			printf("  Version (File):			%x\n", pE->e_version);
			break;
	}

	// ENTRY POINT
	printf("  Entry point address:			0x%2x\n", pE->e_entry);

	// PROGRAM HEADER OFFSET
	printf("  Start of program headers:		%d (bytes into file)\n", pE->e_phoff);

	// SECTION HEADER OFFSET
	printf("  Start of section headers:		%d (bytes into file)\n", pE->e_shoff);

	// FLAGS
	printf("  Flags:				0x%x\n", pE->e_flags);

	// HEADER SIZE
	printf("  Size of this header:			%d\n", pE->e_ehsize);

	// PROGRAM HEADER SIZE
	printf("  Size of program headers:		%d\n", pE->e_phentsize);

	// PROGAM HEADER AMOUNT
	printf("  Number of program headers:		%d\n", pE->e_phnum);

	// SECTION HEADER SIZE
	printf("  Size of section headers:		%d\n", pE->e_shentsize);

	// SECTION HEADER AMOUNT
	printf("  Number of section headers:		%d\n", pE->e_shnum);

	// SECTION HEADER STRING TABLE INDEX
	printf("  Section header string table index:	%d\n", pE->e_shstrndx);

	if (close(fd) < 0)
	{
		perror("c1");
		exit(1);
	}

	#ifdef DEBUG
	printf("DEBUG: Closed the fd.\n\n");
	#endif

	free(buf);
	return 0;
}

