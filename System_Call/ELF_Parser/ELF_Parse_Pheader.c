/*
 * ELF Parser:
 *     File handling with system calls.
 *     This program will open a file (passed in through command line).
 *     It will check the file and then close it.
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <elf.h>

#define DEBUG

//char* convert_type(int phnum);


/****************************************************************
 * Function: usage()
 * Brief: Check that a single argument was passed in
 ****************************************************************/
void usage(int argc)
{
	if (argc != 2)
	{
		fprintf(stderr, "\nThis program takes in one argument-"
			"the file it's meant to read.\n");
		exit(1);
	}
}


/***************************************************************
 *  Function: convert_type()
 *  Brief: Take in an int, return a string
 ***************************************************************
char * convert_type(int phnum)
{
	switch (phnum)
	{
		case 0:
			return "PT_NULL";
			break;
		case 1:
			return "PT_LOAD";
		case 2:
			return "PT_DYNAMIC";
		case 3:
			return "PT_INTERP";
		case 4:
			return "PT_NOTE";
		case 5:
			return "PT_SHLIB";
		case 6:
			return "PT_PHDR";
		case 7:
			return "PT_TLS";
		case 0x60000000:
			return "PT_LOOS";
		case 0x6fffffff:
			return "PT_HIOS";
		case 0x70000000:
			return "PT_LOPROC";
		case 0x7fffffff:
			return "PT_HIPROC";
	}
	return;
}


/***************************************************************
 * Function: main()
 * Brief: Read a binary file
 ***************************************************************/
int main(int argc, char *argv[])
{
	int fd = 0;
	int sz = 0;
	int phnum = 0;
	int i = 0;
//	char * ptype;
	Elf64_Ehdr *pE;
	Elf64_Phdr *pP;
	
	usage(argc);

	// create memory space to read the program headers
	// I want to load 9 because there are 9 segments
	void *buf_1 = calloc(1, sizeof(Elf64_Ehdr)); 
	void *buf_2 = calloc(9, sizeof(Elf64_Phdr));

	// open the file to be read
	fd = open(argv[1], O_RDONLY);

	if (fd == -1) // if the file didn't open fd will be -1)
	{
		perror("cannot open file");
		exit(1);
	}

	// read the ELF headers from the ELF file
	sz = read(fd, buf_1, sizeof(Elf64_Ehdr));
	pE = (Elf64_Ehdr *)buf_1;	

	// grab the number of program headers
	phnum = pE->e_phnum;
	printf("Program Header Count: %d\n", pE->e_phnum);

	// iterate through and print the program headers
	printf("Program Headers:\n  Type		Offset			VirtAddr		PhysAddr\n		FileSiz			MemSiz			Flags	Align\n");
	for (i; i < phnum; i++)
	{
	sz = read(fd, buf_2, sizeof(Elf64_Phdr));
	pP = (Elf64_Phdr *)buf_2;
//	ptype = convert_type(pP->p_type);
	printf("  %x		0x%.16x	0x%.16x	0x%.16x\n		0x%.16x	0x%.16x	%x	%x\n", pP->p_type, pP->p_offset, pP->p_vaddr, pP->p_paddr, pP->p_filesz, pP->p_memsz, pP->p_flags, pP->p_align);

	} 
	
	//printf(" offset : %d\n", pE->e_phoff);
	//printf(" num ; %d\n", pE->e_phnum);

	if (close(fd) < 0)
	{
		perror("c1");
		exit(1);
	}


	free(buf_1);
	free(buf_2);
	return 0;
}

