Create a program that opens a file (passed in through the command line).
- This program should error check the file, and then close it.
- This program should only use linux system calls.

