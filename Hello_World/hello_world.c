#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init hello_init(void)
{
	printk(KERN_INFO "\nHello world\n");

	/*
	 * A non 0 return means init_module failed; module can't be loaded.
	 */
	return 0;
}

static void __exit hello_cleanup(void)
{
	printk(KERN_INFO "\nGoodbye world\n");
}

module_init(hello_init);
module_exit(hello_cleanup);
